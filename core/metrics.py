import abc


class Metric(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def distance(self, *args, **kwargs):
        pass


class SquaredEuclidMetric(Metric):
    @staticmethod
    def distance(vec1, vec2):
        return sum(x**2 + y**2 for x in vec1 for y in vec2)


class ManhattanMetric(Metric):
    @staticmethod
    def distance(vec1, vec2):
        return sum(abs(x - y) for x in vec1 for y in vec2)


class ChebyshevMetric(Metric):
    @staticmethod
    def distance(vec1, vec2, dim=0):
        return abs(vec1[dim] - vec2[dim])


class MinkovskyMetric(Metric):
    @staticmethod
    def distance(vec1, vec2, p=3):
        return sum(abs(x - y) ** p for x in vec1 for y in vec2) ** (1/p)


class EuclidMetric(Metric):
    @staticmethod
    def distance(vec1, vec2):
        return MinkovskyMetric.distance(vec1, vec2, 2)
