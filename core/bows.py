from core.metrics import *
import numpy as np


class Bow:
    def __init__(self):
        self.vector = []

    def normalize(self, metric: Metric):
        zeros = np.zeros(len(self.vector))
        m = metric.distance(self.vector, zeros)
        self.vector = [x / m for x in self.vector]

    def bow(self):
        return self.vector


class BinaryBow(Bow):
    def __init__(self, basis: list, text: list):
        super(BinaryBow).__init__()
        for word in basis:
            if word in text:
                self.vector.append(1)
            else:
                self.vector.append(0)


class CountBow(Bow):
    def __init__(self, basis: list, text: list):
        super(CountBow).__init__()
        count_dict = dict(zip(basis, np.zeros(len(self.vector))))
        for word in text:
            if word in basis:
                count_dict[word] += 1
        for word in basis:
            self.vector.append(count_dict[word])


class TfIdfBow(Bow):
    def __init__(self, basis: list, text: list, list_doc: list):
        super(TfIdfBow).__init__()
        counts = CountBow(basis, text).bow()
        occurences = []
        for word in basis:
            v = 0
            for doc in list_doc:
                if word in doc:
                    v += 1
            occurences.append(v)
        self.vector = [tf * np.log10(len(list_doc)/dt) for tf in counts for dt in occurences]
