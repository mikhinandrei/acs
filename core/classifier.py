from operator import itemgetter
from core.vectorizator import vectorize
import json


def distance(vec1, vec2):
    return sum([(vec1[i] - vec2[i]) ** 2 for i in range(len(vec1))]) ** 0.5


def knnClassify(txt, texts, num_classes):
    dsts = []
    with open('data/basis.json', 'r') as f:
        content = f.readline()
    basis_words = json.loads(content)
    new = vectorize(txt, texts, basis_words)
    for text in texts:
        dsts.append([distance(new, text['vector']), text['class']])
    stat = [0 for _ in range(num_classes)]
    for item in sorted(dsts, key=itemgetter(0))[:num_classes]:
        stat[item[1]] += 1

    return stat.index(max(stat))