import requests
from bs4 import BeautifulSoup
import os
import asyncio
import aiohttp
import time


URL = "https://arxiv.org/"
SUFFIXES = [
    'list/physics/1905?skip=225&show=100',
    'list/math/1905?skip=225&show=100',
    'list/cs/1905?skip=225&show=100',
    'list/q-bio/1905?skip=225&show=100',
    'list/q-fin/1905?skip=175&show=100',
    'list/econ/1905?skip=225&show=100',
]


async def fetch(session, url):
    async with session.get(url) as response:
        return await response.content.read()


async def parse():
    for i in range(len(SUFFIXES)):
        resp = requests.get(URL + SUFFIXES[i])
        if not os.path.exists(str(i)):
            os.makedirs(str(i))

        articles_page = BeautifulSoup(resp.text, "lxml")
        spans = articles_page.find_all('span', class_='list-identifier')[:35]
        for span in spans:
            link = span.find_all('a')[1]['href']
            fname = link.split('/')[-1]
            with open(f'{i}/{fname}.pdf', 'wb') as f:
                async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
                    f.write(await fetch(session, URL + link))
                print(f"Successfully downloaded {i}/{fname}.pdf")


if __name__ == '__main__':
    start = time.time()
    asyncio.run(parse())
    print(time.time() - start)
