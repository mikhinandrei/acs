from pymongo import MongoClient
from os import listdir
from os.path import join, isfile
import textract
import re
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import matplotlib.pyplot as plt
from core.bows import *
from nltk.stem import *


texts = []

client = MongoClient('mongodb://localhost:27017/')
db = client.uir
stemmer = SnowballStemmer('english')

basis = {}
sw = set(stopwords.words('english'))
for i in range(10):
    filenames = [join(str(i), f) for f in listdir(f'{i}') if isfile(join(str(i), f))]
    for filename in filenames:
        try:
            text = textract.process(filename).decode('utf-8')
        except Exception as e:
            print(e)
            print(f'{filename} failed!')
            continue
        text = re.sub(r'-\n', '', text.lower())
        text = re.sub(r'\n', ' ', text)
        text = re.sub(r'[,.]', '', text)
        text = re.sub(r'\s+', ' ', text)
        arr = word_tokenize(text)
        #arr = list(filter(lambda x: len(x) >= 2, arr))
        arr = list(filter(lambda x: x not in sw, arr))
        arr = list(filter(lambda x: len(x) > 2, arr))
        arr = list(filter(lambda x: not re.match(r'\d+$', x), arr))
        arr = list(filter(lambda x: re.match(r'\w+$', x), arr))
        arr = [stemmer.stem(word) for word in arr]
        text_vector = {}
        for w in arr:
            if w not in basis:
                basis[w] = 1
            else:
                basis[w] += 1
            if w not in text_vector.keys():
                text_vector[w] = 1
            else:
                text_vector[w] += 1
        print(f'{filename} processed!')
        print(text_vector)

#print(basis, len(basis), sep='\n')
b = [x for x in list(filter(lambda k: basis[k] >= 25 and basis[k] <= 500, basis.keys))]
print(b)
