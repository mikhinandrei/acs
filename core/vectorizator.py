import re
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.snowball import *
import numpy as np


def extract_pattern(arr):
    if arr:
        return re.sub(r'\\.*', '', arr[0])
    return ''


def vectorize(text, texts, basis_words):
    sw = set(stopwords.words('english'))
    stemmer = SnowballStemmer('english')
    title = extract_pattern(re.findall(r'([\w\s\-,\.]+)\\', text))
    author_email = extract_pattern(re.findall(r'\S+\@\S+', text))
    university = extract_pattern(re.findall(r'([^\\\d,\.]*university[^\\\d,\.]*)\\', text, re.IGNORECASE))
    text = re.sub(r'-\n', '', text.lower())
    text = re.sub(r'\n', ' ', text)
    text = re.sub(r'[,.]', '', text)
    text = re.sub(r'\s+', ' ', text)
    arr = word_tokenize(text)
    arr = [stemmer.stem(word) for word in arr]
    arr = list(filter(lambda x: x not in sw, arr))
    arr = list(filter(lambda x: not re.match(r'\d+$', x), arr))
    arr = list(filter(lambda x: re.match(r'\w+$', x), arr))
    arr = list(filter(lambda x: len(x) > 3, arr))
    arr = list(filter(lambda x: len(x) < 15, arr))
    counts = {}
    res = []
    for w in arr:
        if w not in counts.keys():
            counts[w] = 1
        else:
            counts[w] += 1
    for i in range(len(basis_words)):
        w = basis_words[i]
        dt = 0
        size = sum(counts.values())
        for d in texts:
            if d['vector'][i] != 0:
                dt += 1
        if w in counts.keys():
            res.append(counts[w] / size * np.log10(1 + len(texts) / dt))
        else:
            res.append(0)

    return res
